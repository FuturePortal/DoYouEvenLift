import Header from '../components/Header';
import Meta from '../components/Meta';

const Home = () => (
	<>
		<Meta />
		<Header />
	</>
);

export default Home;
