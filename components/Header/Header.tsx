import styled from './Header.module.scss';

import { ReactElement } from 'react';

const Header = (): ReactElement => (
	<div className={styled.container}>
		<div>
			<h1 className={styled.title}>DO YOU EVEN LIFT?</h1>
		</div>
	</div>
);

export default Header;
