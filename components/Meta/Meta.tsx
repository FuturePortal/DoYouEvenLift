import Head from 'next/head';

import { ReactElement } from 'react';

const Meta = (): ReactElement => (
	<Head>
		<title>Do you even lift?</title>
		<meta name="description" content="Haal het beste uit jezelf, dat kan er maar één doen, en dat ben jij." />
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
		<link rel="manifest" href="/site.webmanifest" />
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#006b89" />
		<link rel="shortcut icon" href="/favicon.ico" />
		<meta name="msapplication-TileColor" content="#111111" />
		<meta name="msapplication-config" content="/browserconfig.xml" />
		<meta name="theme-color" content="#111111"></meta>
	</Head>
);

export default Meta;
