# DoYouEvenLift.nl

This is an open source website to get more information about losing or gaining weight.

## Implement daily calory calculator

- For women, BMR = 655.1 + (9.563 x weight in kg) + (1.850 x height in cm) - (4.676 x age in years)
- For men, BMR = 66.47 + (13.75 x weight in kg) + (5.003 x height in cm) - (6.755 x age in years)
